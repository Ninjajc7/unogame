#PseudoCode
When a game starts, a player connects to the server, they have the
option of connecting as an observer or a player. If this is the 1st player connecting, they send
a “START-JOIN” message to the server to start a game. Other players can send a
“START-JOIN” to the server to join a game but is immediately returned a WAIT message. The
server continues to broadcast WAIT message until the 30seac wait time expires to start the
game. If there are not at least 2 players connected within the wait period, the game ends by
default. Once gameplay starts a broadcasted DECK message is sent to all the players with
their current cards. If other players connect and the wait time has expired, the server will sent
a WAIT message which freezes all the players, observer is permanently frozen since they
cannot play. During each turn, the player will send a REQUEST message to see if the game is
still live, turn ended, server disconnection, or ended. The server gives 15 seconds per turn. If
the player draws a card and wants to end their turn early, they can send a EndTurn message
to the server and end their turn.

#User Actions

##WAIT
1. The action will be “WAIT”. There will be several different parameters.
	1. Frozen- This parameter sends a boolean to the client informing them of a wait or
start.
	2. Frozen is only used in game play for players only, ignores observers.
	3. Frozen can be reset if a player ends a turn earlier than 30 seconds
	4. Players only use and expect this message from the server.
	5. Is “Frozen” the only parameter? If so, do we need to say Frozen or is the WAIT
message enough?

##START-JOIN (client to the server)
1. This will be an application message. The action will be “start”. There will be a single
parameter specifying the game type: “UNO”, “PHASE_TEN”.
	1 Player: Boolean, defaults to true otherwise false for observers.
	2. No username: defaults to observer
	3. Checks before game play start
2. There will also be another parameter specified called player. It is a boolean. If true, the
player wants to join. Otherwise, or if the parameter is not speci뀄ed, the game will treat the
user as an observer. The observer may not draw or play the game.
3. Each player will, on connecting to the server, receive a start message from the server.
	1. For the first player, the server will initialize the deck and wait for additional players.
	2. The Server will send a Request message with a Frozen boolean value of true. It
	will also set an server timer for 30 seconds. Anytime during that time, other
	players may join the game. The Server will only respond to all requests with a
	Frozen : true message. That tells the clients they need to wait.
	3. If, at the end of Thirty seconds, only 1 player has joined, the SERVER sends an exit
	message. This ends the game.
	4. Otherwise, the server sends a WAIT message to the player, telling them to wait
	after a certain amount of time. It also sends a REQUEST message to the player
	and to the Observer, telling them what the status is of the server.
	5. After the time period has passed, the Server determines the order of the players
	(rand) and broadcasts REQUEST messages out to each player.


##REQUEST (Happens at each state of game including turns)
1. “IsLive” - checks to confirm that the server is still running and players are still playing or
the game is live.
	1. Only used during game play and in between turns.
	2. Boolean: determines if the game is still available or dropped connection.
	3. Determines if game has been won
2. PlayerTurn: Array of Objects: checks if current player's turn.
	1. username: Their Name
	2. Boolean: if it’s the player's turn
	3. Player: boolean
	4. Cards: Integer, number of cards you are holding
3. CurrentTurn: boolean is it the current turn
	1. Stays at false and keeps the user waiting until current turn is over.
	2. Set to false when player is
4. Status: “Lobby”, “Playing”, “Win” . Determines the current status the server is in.

##DRAW (sent from client to server, returns to all clients)
1. Card: Card Object -- Will be a dummy card if the client is not playing
2. Username: the name of the player the card is sent to
	1. Used to increment the dummy cards for the client.
3. Hidden: boolean - Observer and current player are set to false
	1. This will go out as a dummy card integer to all players besides the observer and
the current player.
4. An auto draw message will be sent out to the current player if they have taken no action
(draw, play) in 30 seconds.

##PLAY-DISCARD (action messages)
1. Card: Card Object of the type they are playing
2. Each player will have thirty seconds after being told it is their turn to either discard or play
a card.
3. Username - this is a broadcast message, so username will tell the other clients which user
played/discarded

##CARD -- Object
1. Rank - Number between 0-9, or keywords SKIP, REVERSE, DRAW_TWO, WILD, WILD_DRAW_FOUR
2. Color - Keywords BLUE, YELLOW, GREEN, RED . Color should be empty if there is a WILD or
WILD_DRAW_FOUR

##DECK (sent from server to client)
1. Deck: integer / number of cards
2. Play: Integer/ number of cards in play pile

##EndTurn (Client to server)
1. End: true or false Notifies server that a turn has ended and to move onto the next user.