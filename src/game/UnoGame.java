package game;

import java.awt.EventQueue;

public class UnoGame
	{
		static MainWindow mainWindow;
		static GameWindow gameWindow;
		static ObserverWindow observWindow;
		static PlayingWindow playWindow;
		
		public static void main(String[] args)
			{
				
				
				EventQueue.invokeLater(new Runnable()
					{
						public void run()
							{
								try
									{
										playWindow = new PlayingWindow();
										mainWindow = new MainWindow(gameWindow, observWindow, playWindow);
										gameWindow = new GameWindow(mainWindow, observWindow);
										gameWindow.setVisible(true);
										
									} catch (Exception e)
									{
										e.printStackTrace();
									}
							}
					});
			}
	}
