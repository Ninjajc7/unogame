package game;

import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.ImageIcon;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.UnknownHostException;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class MainWindow extends JPanel
	{
		
	private static final long serialVersionUID = -7049792360401453948L;
		private JPanel mainWindowPanel;
		public PlayingWindow pw;
		private JLabel rickRollLabel = new JLabel("");
		private JLabel backgroundLabel = new JLabel("");
		private JTextField servAddressBox;
		private JTextField servePortbox;
		public NetworkConnections servConn;
		private int serverPortNumInt;
		public String amazonAddress = "ec2-52-43-242-239.us-west-2.compute.amazonaws.com";
		private JTextField userNameBox;
		

		
		public MainWindow(GameWindow gw, ObserverWindow ow, PlayingWindow pw)
			{	
				this.setBackground(new Color(0, 0, 0));
				
				//set screen size
				this.setSize(1500, 1000);
				
				//centers window on screen
				//this.setLocationRelativeTo(null);  
				//mainFrame.setBounds(0, 0, 450, 300);
				
				//this.setde(JFrame.EXIT_ON_CLOSE);
				this.setLayout(null);
				
				mainWindowPanel = new JPanel();
				mainWindowPanel.setBackground(new Color(0, 0, 0));
				mainWindowPanel.setBounds(10, 11, 1474, 650);
				this.add(mainWindowPanel);
				mainWindowPanel.setLayout(null);
				
				/////mushroom background/////
				backgroundLabel.setBounds(0, 0, 1280, 1024);
				this.add(backgroundLabel);
				backgroundLabel.setIcon(new ImageIcon(MainWindow.class.getResource("/images/mushroomBackground.jpg")));
				//END mushroom background

				JLabel serverIPlabel = new JLabel("Server IP: ");
				serverIPlabel.setFont(new Font("Tahoma", Font.PLAIN, 17));
				serverIPlabel.setBackground(new Color(255, 255, 255));
				serverIPlabel.setForeground(new Color(255, 255, 255));
				serverIPlabel.setBounds(79, 32, 78, 21);
				mainWindowPanel.add(serverIPlabel);
				
				//default server address here
				servAddressBox = new JTextField(amazonAddress);
				servAddressBox.setBounds(154, 35, 177, 20);
				mainWindowPanel.add(servAddressBox);
				servAddressBox.setColumns(10);
				
				//server status label
				JLabel servConnStatus = new JLabel("NOT CONNECTED");
				servConnStatus.setHorizontalAlignment(JLabel.CENTER);
				servConnStatus.setFont(new Font("Trebuchet MS", Font.BOLD | Font.ITALIC, 14));
				servConnStatus.setForeground(Color.RED);
				servConnStatus.setBounds(154, 66, 177, 14);
				mainWindowPanel.add(servConnStatus);
				
				JLabel lblPort = new JLabel("Port: ");
				lblPort.setForeground(Color.WHITE);
				lblPort.setFont(new Font("Tahoma", Font.PLAIN, 17));
				lblPort.setBackground(Color.WHITE);
				lblPort.setBounds(356, 32, 41, 21);
				mainWindowPanel.add(lblPort);
				
				servePortbox = new JTextField("8989");
				servePortbox.setColumns(10);
				servePortbox.setBounds(396, 35, 86, 20);
				mainWindowPanel.add(servePortbox);
				
				/////Username
				
				
				/////UNObutton 
				JButton unoButton = new JButton("");
				unoButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mousePressed(MouseEvent arg0) {
						System.out.println("I Started an UNO game!");
						gw.startUnoGame(gw, pw, servConn);
					}
				});
				unoButton.setIcon(new ImageIcon(MainWindow.class.getResource("/images/uno.JPG")));
				unoButton.setBounds(79, 158, 292, 300);
				mainWindowPanel.add(unoButton);
				//END UnoButton
				
				/////Phase10 Button
				JButton phase10button = new JButton("");
				phase10button.setIcon(new ImageIcon(MainWindow.class.getResource("/images/phase10.jpeg")));
				phase10button.setBounds(424, 158, 292, 300);
				mainWindowPanel.add(phase10button);
				//END Phase10 Button
				
				/////Rick Roll mouse click here/////
				JButton rickRollBtn = new JButton("");
				rickRollBtn.addMouseListener(new MouseAdapter() {
					@Override
					public void mousePressed(MouseEvent arg0) {
						System.out.println("I clicked DON'T PUSH");
						try
							{
								new BGmusic().rickRoll();
								rickRollLabel.setIcon(new ImageIcon(MainWindow.class.getResource("/images/rickroll.jpg")));
								rickRollLabel.setBounds(750, 100, 500, 500);
								rickRollLabel.setVisible(false);
								mainWindowPanel.add(rickRollLabel);
								rickRollLabel.setVisible(true);
							} catch (InterruptedException e)
							{
								e.printStackTrace();
							}
					}
				});
				rickRollBtn.setIcon(new ImageIcon(MainWindow.class.getResource("/images/DontPush.jpg")));
				rickRollBtn.setBounds(356, 503, 89, 71);
				mainWindowPanel.add(rickRollBtn);
				//END Rick roll
				
				/////Observer///// 
				JButton btnObserve = new JButton("OBSERVE GAME");
				btnObserve.setFont(new Font("Tahoma", Font.PLAIN, 18));
				btnObserve.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						//observer action here
						System.out.println("I clicked on Observer");
						gw.startObservGame(gw, ow);
					}
				});
				btnObserve.setBounds(306, 469, 188, 23);
				mainWindowPanel.add(btnObserve);
				//END observer
				
				
				//connect to server
				JButton btnConnect = new JButton("CONNECT");
				btnConnect.addMouseListener(new MouseAdapter() {
					@Override
					public void mousePressed(MouseEvent arg0) {
						
						serverPortNumInt = Integer.parseInt(servePortbox.getText());
						try
							{
								servConn = new NetworkConnections(servAddressBox.getText(), serverPortNumInt, userNameBox.getText(), pw);
								if(servConn.status() == true)
									{
										servConnStatus.setText("CONNECTED");
										servConnStatus.setForeground(Color.green);
									}
							} catch (UnknownHostException e)
							{
								e.printStackTrace();
							} catch (IOException e)
							{
								e.printStackTrace();
							}
					}
				});
				btnConnect.setBounds(520, 34, 89, 23);
				mainWindowPanel.add(btnConnect);
				
				userNameBox = new JTextField("userName");
				userNameBox.setColumns(10);
				userNameBox.setBounds(344, 94, 86, 20);
				mainWindowPanel.add(userNameBox);
				
				JLabel lblUsername = new JLabel("Username: ");
				lblUsername.setForeground(Color.WHITE);
				lblUsername.setFont(new Font("Tahoma", Font.PLAIN, 17));
				lblUsername.setBackground(Color.WHITE);
				lblUsername.setBounds(252, 91, 104, 21);
				mainWindowPanel.add(lblUsername);
				

				//get server String address

			}
		
		public String getServerString()
			{
				return amazonAddress;
			}
		
		public NetworkConnections getServConn()
			{
				return servConn;
			}
	}
