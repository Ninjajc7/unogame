package game;

import org.json.JSONObject;

public class MessageFactory
	{
		public JSONObject startGame()
		{
			//my root type
			JSONObject startGame = new JSONObject();
			startGame.put("type", "application");
			
			//messages in object
			JSONObject message = new JSONObject();
			message.put("module", "uno");
			message.put("action", "start");
			startGame.put("message", message);
			
			return startGame;
		}
		
		public JSONObject chatMessage(String myMessage, String username)
		{
			//my root type
			JSONObject chatMessage = new JSONObject();
			chatMessage.put("type", "chat");
			chatMessage.put("message", myMessage);
	
			return chatMessage;
		}
		
		public void decodeJSONtype(JSONObject json)
		{
			  String type;
			  //JSONObject message;
			  
			  if(json.has("type"))
				  {
					  type = json.getString("type");
					  System.out.println("JSON Type: " + type);
				  }
			  if(json.has("action"))
				  {
					  type = json.getString("action");
					  System.out.println("JSON Type: " + type);
					  //message = json.getJSONObject("message");
					  //System.out.println("JSON Type: ");
					  
				  }
			  //System.out.println("JSON Msg: " + message);
		}
			
	}
