package game;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class GameWindow extends JFrame
	{
		private static final long serialVersionUID = -8840122984894667711L;

		//public GameWindow(JPanel mWindow)
		public GameWindow(JPanel mWindow, JPanel oWindow)
		{
			//set the size	
			setSize(1500, 1000);	
				
			//center screen
			setLocationRelativeTo(null);  
			
			//kills everything on default close
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			add(new MainWindow(this, new ObserverWindow(), new PlayingWindow()));
		}
		
		public void startUnoGame(GameWindow gw, PlayingWindow pw, NetworkConnections servConn)
		{
			System.out.println("Inside the GameWindow class, startUnoGame()");
			gw.getContentPane().removeAll();
			System.out.println("adding playing panel");
			gw.validate();
			gw.add(pw);
			gw.validate();
			gw.repaint();
			servConn.sendMessage(new MessageFactory().startGame());
		}
		
		public void startObservGame(GameWindow gw, ObserverWindow ow)
		{
			System.out.println("Inside the GameWindow class, startObservGame()");
			gw.getContentPane().removeAll();
			System.out.println("adding observe panel");
			gw.validate();
			gw.add(ow);
			gw.validate();
			gw.repaint();
		}
		
		

	}
