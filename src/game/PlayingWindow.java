package game;

import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;


public class PlayingWindow extends JPanel
	{
		
	public ChatBox chatBox;
	
	
	private static final long serialVersionUID = 1853697800561389430L;

		public PlayingWindow()
			{
				//set size same as GameWindow
				setSize(1500, 1000);
				
				//default to center window
				setLayout(null);
				
				//set size same as GameWindow
				setSize(1500, 1000);
				
				//default to center window
				setLayout(null);
				
				//set background to black 
				this.setBackground(new Color(0, 0, 0));
				
				JLabel lblNumOfCards = new JLabel("NUM OF CARDS IN HAND");
				lblNumOfCards.setForeground(Color.WHITE);
				lblNumOfCards.setFont(UIManager.getFont("TextArea.font"));
				lblNumOfCards.setBounds(38, 11, 175, 18);
				add(lblNumOfCards);
				
				//player1
				JLabel p1Label = new JLabel("Player 1: ");
				p1Label.setForeground(Color.WHITE);
				p1Label.setFont(new Font("Tahoma", Font.PLAIN, 18));
				p1Label.setBounds(10, 40, 75, 22);
				add(p1Label);
				
				JButton p1Cards = new JButton("");
				p1Cards.setBounds(95, 42, 64, 20);
				add(p1Cards);
				p1Cards.setEnabled(false);
				p1Cards.setText("");
				
				
				//player2
				JLabel p2label = new JLabel("Player 2: ");
				p2label.setForeground(Color.WHITE);
				p2label.setFont(new Font("Tahoma", Font.PLAIN, 18));
				p2label.setBounds(10, 73, 75, 22);
				add(p2label);
				
				JButton p2Cards = new JButton("");
				p2Cards.setBounds(95, 75, 64, 20);
				add(p2Cards);
				p2Cards.setEnabled(false);
				p2Cards.setText("");
				
				//player3
				JLabel p3label = new JLabel("Player 3: ");
				p3label.setForeground(Color.WHITE);
				p3label.setFont(new Font("Tahoma", Font.PLAIN, 18));
				p3label.setBounds(10, 106, 75, 22);
				add(p3label);
				
				JButton p3Cards = new JButton("");
				p3Cards.setBounds(95, 108, 64, 20);
				add(p3Cards);
				p3Cards.setEnabled(false);
				p3Cards.setText("");
				
				
				//player4
				JLabel p4label = new JLabel("Player 4: ");
				p4label.setForeground(Color.WHITE);
				p4label.setFont(new Font("Tahoma", Font.PLAIN, 18));
				p4label.setBounds(10, 139, 75, 22);
				add(p4label);
			
				JButton p4cards = new JButton("");
				p4cards.setBounds(95, 141, 64, 20);
				add(p4cards);
				p4cards.setEnabled(false);
				p4cards.setText("");
						
						//BG music on request
						JLabel lblMusic = new JLabel("Click for music");
						lblMusic.setFont(new Font("Tahoma", Font.PLAIN, 30));
						lblMusic.setForeground(Color.WHITE);
						lblMusic.setBounds(38, 186, 227, 80);
						add(lblMusic);
						
						JButton btnMusic = new JButton("");
						btnMusic.addMouseListener(new MouseAdapter() {
							@Override
							public void mousePressed(MouseEvent arg0) {
								try
									{
										new BGmusic().playThinkMusic();
									} catch (InterruptedException e)
									{
										e.printStackTrace();
									}
							}
						});
						btnMusic.setIcon(new ImageIcon(PlayingWindow.class.getResource("/images/music.jpg")));
						btnMusic.setBounds(70, 267, 143, 134);
						add(btnMusic);
						
						//add chat box panel to playing Window. 
						chatBox = new ChatBox();
						add(chatBox);
						
						JButton card5 = new JButton("");
						card5.setIcon(new ImageIcon(PlayingWindow.class.getResource("/images/front_card_sm.png")));
						card5.setBounds(1197, 12, 99, 134);
						add(card5);
						
						JButton card6 = new JButton("");
						card6.setIcon(new ImageIcon(PlayingWindow.class.getResource("/images/front_card_sm.png")));
						card6.setBounds(1340, 11, 99, 134);
						add(card6);
						
						JButton card1 = new JButton("");
						card1.setIcon(new ImageIcon(PlayingWindow.class.getResource("/images/front_card_sm.png")));
						card1.setBounds(625, 11, 99, 134);
						add(card1);
						
						JButton card2 = new JButton("");
						card2.setIcon(new ImageIcon(PlayingWindow.class.getResource("/images/front_card_sm.png")));
						card2.setBounds(768, 11, 99, 134);
						add(card2);
						
						JButton card3 = new JButton("");
						card3.setIcon(new ImageIcon(PlayingWindow.class.getResource("/images/front_card_sm.png")));
						card3.setBounds(911, 11, 99, 134);
						add(card3);
						
						JButton card4 = new JButton("");
						card4.setIcon(new ImageIcon(PlayingWindow.class.getResource("/images/front_card_sm.png")));
						card4.setBounds(1054, 12, 99, 134);
						add(card4);
						
						JButton card11 = new JButton("");
						card11.setIcon(new ImageIcon(PlayingWindow.class.getResource("/images/front_card_sm.png")));
						card11.setBounds(1197, 173, 99, 151);
						add(card11);
						
						JButton card8 = new JButton("");
						card8.setIcon(new ImageIcon(PlayingWindow.class.getResource("/images/front_card_sm.png")));
						card8.setBounds(768, 173, 99, 151);
						add(card8);
						
						JButton card7 = new JButton("");
						card7.setIcon(new ImageIcon(PlayingWindow.class.getResource("/images/front_card_sm.png")));
						card7.setBounds(625, 173, 99, 151);
						add(card7);
						
						JButton card9 = new JButton("");
						card9.setIcon(new ImageIcon(PlayingWindow.class.getResource("/images/front_card_sm.png")));
						card9.setBounds(911, 173, 99, 151);
						add(card9);
						
						JButton card12 = new JButton("");
						card12.setIcon(new ImageIcon(PlayingWindow.class.getResource("/images/front_card_sm.png")));
						card12.setBounds(1340, 173, 99, 151);
						add(card12);
						
						JButton card10 = new JButton("");
						card10.setIcon(new ImageIcon(PlayingWindow.class.getResource("/images/front_card_sm.png")));
						card10.setBounds(1054, 173, 99, 151);
						add(card10);
			}
	}
