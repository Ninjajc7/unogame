package game;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;


import org.json.*;
public class NetworkConnections implements Runnable
	{
	
		private BufferedReader in;
		public PrintWriter out;
		public Socket s;
		private boolean status = false;
		private String userName;
		public PlayingWindow pw;
		MessageFactory msgfactory = new MessageFactory();
		
		public NetworkConnections(String servAddress, int servPort, String user, PlayingWindow pw) throws UnknownHostException, IOException
		{
			//server connection info
			this.pw = pw;
			s = new Socket(servAddress, servPort);
			this.userName = user;
			Thread thread = new Thread(this);
			thread.start();
			out = new PrintWriter(s.getOutputStream(), true);
			login();
			JFrame frame = null;
			JOptionPane.showMessageDialog(frame, "Connection to Server successful!");
			System.out.println("End network constructor, socket created\n");
			status = true;
		}
		
		public void login()
		{
			//rootType
			JSONObject json = new JSONObject();
			json.put("type", "login");
			
			//message type
			JSONObject message = new JSONObject();
			message = message.put("username", userName);
			
			json.put("message", message);
			sendMessage(json);
		}
		
		public void sendMessage(JSONObject json)
		{
			this.out.println(json);
			
			//show what's being sent
			System.out.println("Json message "+json.toString());
			
			//flush the stream
			this.out.flush();
		}
		
		public boolean status()
		{
			return status;
		}

		@Override
		public void run()
			{
				//System.out.println("In the run method");
				try
					{
						in = new BufferedReader(new InputStreamReader(s.getInputStream()));		
						//System.out.println("Trying to read from server");
					} catch (IOException e)
					{
						e.printStackTrace();
					}
				while (true)
					{
						try
							{
								//get text from chat window
								System.out.println(pw.chatBox.userChatText());
								
								//output messages from server to console
								String input = in.readLine();
								System.out.println(input);
								
								//find out the type of JSON Object
								msgfactory.decodeJSONtype(new JSONObject(input));
								
								//send text to chat window
								pw.chatBox.writeToChat(input.toString());
								
								//get chat from text box to be sent
								
								
							} catch (IOException e)
							{
								e.printStackTrace();
							}
					}
			}

}
