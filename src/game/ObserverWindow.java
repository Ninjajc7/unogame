package game;

import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.UIManager;
import javax.swing.JPanel;


public class ObserverWindow extends JPanel
	{

	private static final long serialVersionUID = 808167475878579706L;

		ObserverWindow()
			{
				
				//set size same as GameWindow
				setSize(1500, 1000);
				
				//default to center window
				setLayout(null);
				
				
				JLabel lblNumOfCards = new JLabel("NUM OF CARDS IN HAND");
				lblNumOfCards.setFont(UIManager.getFont("TextArea.font"));
				lblNumOfCards.setBounds(38, 11, 175, 18);
				add(lblNumOfCards);
				
				//player1
				JLabel p1Label = new JLabel("Player 1: ");
				p1Label.setFont(new Font("Tahoma", Font.PLAIN, 18));
				p1Label.setBounds(10, 40, 75, 22);
				add(p1Label);
				
				JButton p1Cards = new JButton("");
				p1Cards.setBounds(95, 42, 64, 20);
				add(p1Cards);
				p1Cards.setEnabled(false);
				p1Cards.setText("");
				
				
				//player2
				JLabel p2label = new JLabel("Player 2: ");
				p2label.setFont(new Font("Tahoma", Font.PLAIN, 18));
				p2label.setBounds(10, 73, 75, 22);
				add(p2label);
				
				JButton p2Cards = new JButton("");
				p2Cards.setBounds(95, 75, 64, 20);
				add(p2Cards);
				p2Cards.setEnabled(false);
				p2Cards.setText("");
				
				//player3
				JLabel p3label = new JLabel("Player 3: ");
				p3label.setFont(new Font("Tahoma", Font.PLAIN, 18));
				p3label.setBounds(10, 106, 75, 22);
				add(p3label);
				
				JButton p3Cards = new JButton("");
				p3Cards.setBounds(95, 108, 64, 20);
				add(p3Cards);
				p3Cards.setEnabled(false);
				p3Cards.setText("");
				
				
				//player4
				JLabel p4label = new JLabel("Player 4: ");
				p4label.setFont(new Font("Tahoma", Font.PLAIN, 18));
				p4label.setBounds(10, 139, 75, 22);
				add(p4label);
			
				JButton p4cards = new JButton("");
				p4cards.setBounds(95, 141, 64, 20);
				add(p4cards);
				p4cards.setEnabled(false);
				p4cards.setText("");
				
				
				//add chat box to observer window
				add(new ChatBox());
				
			}
	}
