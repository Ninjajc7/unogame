package game;

import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class ChatBox extends JPanel
	{
		/**
	 * generated serial version
	 */
	private static final long serialVersionUID = 3282065796621979412L;
		public JTextArea chatBox;
		public JTextArea textInput;
		//public MainWindow mw = new MainWindow();
		//public NetworkConnections servConn = mw.getServConn();
		
		public ChatBox()
			{
				setBounds(25, 435, 545, 488);
				setLayout(null);
				
				//label for the text box
				JLabel chatRspLbl = new JLabel("TYPE RESPONSES HERE");
				chatRspLbl.setFont(new Font("Tahoma", Font.PLAIN, 9));
				chatRspLbl.setBounds(22, 463, 114, 14);
				add(chatRspLbl);
				
				//scroll pane for chat box
				JScrollPane scrollPaneChat = new JScrollPane();
				scrollPaneChat.setBounds(10, 11, 525, 411);
				add(scrollPaneChat);
				
				chatBox = new JTextArea();
				chatBox.setLineWrap(true);
				scrollPaneChat.setViewportView(chatBox);
				chatBox.setColumns(10);
				textInput = new JTextArea(1,50);
				textInput.setWrapStyleWord(true);
				textInput.setBounds(146, 458, 276, 22);
				add(textInput);
				textInput.setTabSize(7);
				textInput.setLineWrap(true);
				
				//sending responses to chat window
				JButton btnSubmit = new JButton("SUBMIT");
				btnSubmit.addMouseListener(new MouseAdapter() {
						@Override
						public void mousePressed(MouseEvent e) {
						int pos = chatBox.getCaretPosition(); //get the cursor position
						String input = textInput.getText();
						String chatText = "Player says: "+input+"\n"; 
						//System.out.println("Player says: " + chatText + "\n");
						chatBox.insert(chatText, pos);
						textInput.setText("");
						}
					});
				
				btnSubmit.setBounds(432, 459, 89, 23);
				add(btnSubmit);
				
				JLabel lblChatBox = new JLabel("CHAT BOX");
				lblChatBox.setBounds(236, 433, 65, 14);
				add(lblChatBox);
			}
		
		public void writeToChat(String message)
		{
	        int pos = chatBox.getCaretPosition(); //get the cursor position
	        chatBox.insert(message+"\n", pos); //insert your text message
	        
		}
		
		public String userChatText()
		{
			String userText = "";
			userText = textInput.getText();
			return userText;	
		}

	}
