package game;
import java.applet.Applet;
import java.applet.AudioClip;
import java.net.URL;

public class BGmusic
	{
		BGmusic() throws InterruptedException
		{

		}
		
		public void playThinkMusic()throws InterruptedException
		{
			URL url = BGmusic.class.getResource("/music/bgMusic.wav");
			AudioClip clip = Applet.newAudioClip(url);
			clip.play();
		}
		
		public void rickRoll()throws InterruptedException
		{
			URL url = BGmusic.class.getResource("/music/NeverGonnaGiveYouUp.wav");
			AudioClip clip = Applet.newAudioClip(url);
			clip.play();
		}
	}
